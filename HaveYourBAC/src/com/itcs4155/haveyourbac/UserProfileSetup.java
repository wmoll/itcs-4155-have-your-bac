package com.itcs4155.haveyourbac;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class UserProfileSetup extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile_setup);
		
		final EditText weight = (EditText)findViewById(R.id.weight);
		Button submitProfile = (Button)findViewById(R.id.submitProfile);
		
		
		
		submitProfile.setOnClickListener(new View.OnClickListener(){
	    	
	    	public void onClick(View view){
	    		
	    		
	    		RadioGroup rg1 = (RadioGroup)findViewById(R.id.gender);
	    		
	    		    int id= rg1.getCheckedRadioButtonId();
	    		    View radioButton = rg1.findViewById(id);
	    		    int radioId = rg1.indexOfChild(radioButton);
	    		    RadioButton btn = (RadioButton) rg1.getChildAt(radioId);
	    		    String selection = (String) btn.getText();
	    		
	    		Intent intent = new Intent(getBaseContext(), Calculator.class);
	    		String weightNum = weight.getText().toString();
	            intent.putExtra("Weight",weightNum);
	            intent.putExtra("Gender",selection);
	    		startActivity(intent);
	    	}
	    
	  });
	    }
	}
