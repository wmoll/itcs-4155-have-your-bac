package com.itcs4155.haveyourbac;

import java.text.DecimalFormat;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Calculator extends Activity {
	double ratio = 0;
	//double alcohol = 0.28;
	double timeTaken = 0.5;
	double bac;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calculator);
		
		TextView weightTextView = (TextView)findViewById(R.id.weightNum);
		TextView genderTextView = (TextView)findViewById(R.id.gender);
		final String weightString = this.getIntent().getStringExtra("Weight");
		String genderString = this.getIntent().getStringExtra("Gender");
		weightTextView.setText(weightString);
		genderTextView.setText(genderString);
		Button calculate = (Button)findViewById(R.id.button1);
		
		if(genderString.equals("Male")){
			ratio = 0.73;
		} else{
			ratio = 0.66;
		}
		
		
		
		calculate.setOnClickListener(new View.OnClickListener(){
	    	
	    	public void onClick(View view){
	    		EditText numDrinks = (EditText)findViewById(R.id.editText1);
	    		double numberOfDrinks = Double.parseDouble(numDrinks.getText().toString());
	    		double alcohol = numberOfDrinks * 0.48;
	    		double weight = Double.parseDouble(weightString);
	    		TextView bacTextView = (TextView)findViewById(R.id.textView5);
	    		
	    		bac = (alcohol * (5.14/(weight * ratio))) - (.015 * timeTaken);
	    		if(bac>=1.00){
	    			String bacString = "He's Dead Jim!";
	    			bacTextView.setText(bacString);
	    		}
	    		else{
	    			String bacString = String.valueOf(bac);
	    			bacTextView.setText(bacString);
	    		}
	    		
	    		
	    	}
	    
	  });

	}
}
