package com.itcs4155.haveyourbac;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    
    final Button login = (Button)findViewById(R.id.login);
    
    final EditText usernameText = (EditText)findViewById(R.id.usernameEntry);
    final EditText passwordText = (EditText)findViewById(R.id.passwordEntry);
    
    	    usernameText.addTextChangedListener(new TextWatcher() {

    	    	
    	        @Override
    	        public void onTextChanged(CharSequence s, int start, int before, int count) {
    	            //Check if 's' is empty 
    	        	

    	        }

    	        @Override
    	        public void beforeTextChanged(CharSequence s, int start, int count,
    	                int after) {
    	            // TODO Auto-generated method stub

    	        }

    	        @Override
    	        public void afterTextChanged(Editable s) {
    	            // TODO Auto-generated method stub
    	        	
    	        }
    	    });
    	    
    	    passwordText.addTextChangedListener(new TextWatcher() {

    	    	   public void afterTextChanged(Editable s) {
    	    		
    	    	   }

    	    	   public void beforeTextChanged(CharSequence s, int start,
    	    	     int count, int after) {
    	    	   }

    	    	   public void onTextChanged(CharSequence s, int start,
    	    	     int before, int count) {
    	    		   
    	    		   if(!usernameText.getText().toString().isEmpty()){
    	    	     login.setEnabled(true);
    	    	   }
    	    	   }
    	    	  });
    
    login.setOnClickListener(new View.OnClickListener(){
    	
    	public void onClick(View view){
    		Intent intent = new Intent(MainActivity.this, UserProfileSetup.class);
    		startActivity(intent);
    	}
    
  });
    }
}
